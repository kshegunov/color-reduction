#ifndef CRCONTROLLER_H
#define CRCONTROLLER_H

#include <QObject>
#include <QImage>

template <class T>
constexpr inline bool qInRange(const T & left, const T & value, const T & right)
{
    return left <= value && value <= right;
}

class QLabel;
class CRController : public QObject
{
    Q_OBJECT

private:
    typedef QVector<QRgb> QColorVector;

public:
    explicit CRController(QObject * parent = 0);

    void setMainWindow(QWidget *);
    void setSource(QLabel *);
    void setDestination(QLabel *);

signals:
    void progressChanged(int);
    void processingFinished();

public slots:
    void loadImage();

private:
    Q_INVOKABLE void createPalette(qint32);
    Q_INVOKABLE void colorReduce();

    void splitBin(QColorVector::Iterator start, QColorVector::Iterator end, qint32 remaining, const qint32 bins);
    QColor colorFromPalette(QRgb);

private:
    QWidget * mainWindow;
    QLabel * source;
    QLabel * destination;

    qint32 progress;    // In %

    QImage image;
    QVector<QColor> palette;
};

inline void CRController::setMainWindow(QWidget * win)
{
    mainWindow = win;
}

inline void CRController::setSource(QLabel * src)
{
    source = src;
}

inline void CRController::setDestination(QLabel * dst)
{
    destination = dst;
}

#endif // CRCONTROLLER_H
