#include "crcontroller.h"
#include "ui_palettesize.h"
#include "ui_progress.h"

#include <QFileDialog>
#include <QVector3D>

#include <cmath>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/mean.hpp>

CRController::CRController(QObject * parent)
    : QObject(parent), mainWindow(nullptr), source(nullptr), destination(nullptr)
{
}

void CRController::loadImage()
{
    Q_ASSERT_X(source && destination && mainWindow, __FUNCTION__, "CRController not initialized properly");

    // Get the image file
    QString fileName = QFileDialog::getOpenFileName(mainWindow, QStringLiteral("Select image ..."), QDir::homePath(), QStringLiteral("Images (*.png *.xpm *.jpg)"));
    if (fileName.isEmpty() || !QFile::exists(fileName))
        return;

    if (!image.load(fileName))
        return; // Can't load

    source->setPixmap(QPixmap::fromImage(image));

    // Select the number of colors
    QDialog paletteSizeDialog(mainWindow);
    paletteSizeDialog.setWindowTitle(QStringLiteral("Select palette size ..."));

    Ui::PaletteSize paletteSize;
    paletteSize.setupUi(&paletteSizeDialog);

    if (paletteSizeDialog.exec() == QDialog::Rejected)
        return;

    QDialog * progressDialog = new QDialog(mainWindow, Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);

    Ui::Progress progress;
    progress.setupUi(progressDialog);

    progressDialog->setModal(true);
    progressDialog->setWindowTitle(QStringLiteral("Processing image ..."));
    progressDialog->show();

    QObject::connect(this, &CRController::progressChanged, progress.progressBar, &QProgressBar::setValue);
    QObject::connect(this,&CRController::processingFinished, progressDialog, &QDialog::deleteLater);

    QMetaObject::invokeMethod(this, "createPalette", Qt::QueuedConnection, Q_ARG(qint32, paletteSize.paletteSize->value()));
}

void CRController::createPalette(qint32 paletteSize)
{
    qint32 width = image.width(), height = image.height(), n = width * height;
    QColorVector colors(n);
    for (qint32 y = 0, i = 0; y < height; y++)  {
        for (qint32 x = 0; x < width; x++, i++)
            colors[i] = image.pixel(x, y);
    }

    auto last = std::unique(colors.begin(), colors.end());
    colors.erase(last, colors.end());

    progress = 5;
    emit progressChanged(progress);

    splitBin(colors.begin(), colors.end(), paletteSize, paletteSize);

    QMetaObject::invokeMethod(this, "colorReduce", Qt::QueuedConnection);
}

void CRController::colorReduce()
{
    progress = 55;
    emit progressChanged(progress);

    QCoreApplication::processEvents();

    // Go through the picture and select the color from the palette
    for (qint32 y = 0, width = image.width(), height = image.height(), i = 0, n = width * height; y < height; y++)  {
        for (qint32 x = 0; x < width; x++, i++)  {
            image.setPixelColor(x, y, colorFromPalette(image.pixel(x, y)));

            progress += 45 * i / n;
            emit progressChanged(progress);

            QCoreApplication::processEvents();
        }
    }

    // And we are finally done. Show the result
    destination->setPixmap(QPixmap::fromImage(image));

    emit processingFinished();
}

QColor CRController::colorFromPalette(QRgb color)
{
    auto colorDistance = [] (const QColor & a, QRgb b) -> qreal  {
        return QVector3D(a.red() - qRed(b), a.green() - qGreen(b), a.blue() - qBlue(b)).length();
    };

    qint32 index = 0;
    qreal minDistance = colorDistance(palette[0], color);

    for (qint32 i = 1, size = palette.size(); i < size; i++)  {
        qreal distance = colorDistance(palette[i], color);
        if (distance < minDistance)  {
            minDistance = distance;
            index = i;
        }
    }

    return palette[index];
}

void CRController::splitBin(QColorVector::Iterator start, QColorVector::Iterator end, qint32 remaining, const qint32 bins)
{
    QCoreApplication::processEvents();

    enum {
        Red, Green, Blue
    } channel;

    qint32 size = 0;

    // Stat the bucket
    using namespace boost::accumulators;
    accumulator_set<qreal, features<tag::min, tag::max, tag::mean(immediate)>> red, green, blue;
    for (QColorVector::Iterator i = start; i != end; ++i, size++)  {
        red(qRed(*i));
        green(qGreen(*i));
        blue(qBlue(*i));
    }

    QCoreApplication::processEvents();

    // Determine the significant channel
    qint32 redRange = max(red) - min(red), greenRange = max(green) - min(green), blueRange = max(blue) - min(blue);
    if (redRange >= greenRange && redRange >= blueRange)
        channel = Red;
    else if(greenRange >= blueRange)
        channel = Green;
    else
        channel = Blue;

    // Sort the samples
    std::sort(start, end, [channel] (QRgb a, QRgb b) -> bool {
        switch (channel)
        {
        case Red:
            return qRed(a) < qRed(b);
        case Green:
            return qGreen(a) < qGreen(b);
        case Blue:
        default:
            return qBlue(a) < qBlue(b);
        }
    });

    QCoreApplication::processEvents();

    if (remaining <= 1)  {      // Add the averaged color to the palette and stop the recursion
        palette.append(QColor(mean(red), mean(green), mean(blue)));

        progress += 50 / bins;
        emit progressChanged(progress);

        return;
    }

    QColorVector::Iterator midpoint = start + (size >> 1);
    splitBin(start, midpoint - 1, remaining >> 1, bins);
    splitBin(midpoint, end, remaining >> 1, bins);
}
