#-------------------------------------------------
#
# Project created by QtCreator 2016-10-27T16:06:32
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = color-reduction
TEMPLATE = app

SOURCES += main.cpp \
    crcontroller.cpp

HEADERS  += \
    crcontroller.h

FORMS += \
    crmainwindow.ui \
    palettesize.ui \
    progress.ui
