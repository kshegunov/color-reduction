#include "crcontroller.h"
#include "ui_crmainwindow.h"

#include <QApplication>
#include <QMainWindow>

int main(int argc, char *argv[])
{
    QApplication application(argc, argv);

    QMainWindow mainWindow;
    mainWindow.show();

    Ui::CRMainWindow ui;
    ui.setupUi(&mainWindow);

    CRController controller(&application);
    controller.setMainWindow(&mainWindow);
    controller.setSource(ui.sourceImage);
    controller.setDestination(ui.destinationImage);

    QObject::connect(ui.actionLoadImage, &QAction::triggered, &controller, &CRController::loadImage);

    return QApplication::exec();
}
